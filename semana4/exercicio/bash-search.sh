#!/usr/bin/env bash

url_base='https://www.gnu.org/savannah-checkouts/gnu/bash/manual/html_node/'

url_index[0]="Builtin-Index.html"
url_index[1]="Reserved-Word-Index.html"
url_index[2]="Variable-Index.html"
url_index[3]="Function-Index.html"
url_index[4]="Concept-Index.html"

i=0

while [[ $i -lt 5 ]]; do
    wget $url_base${url_index[i++]} -O /tmp/total$i.html
    sed -n '/Index Entry/,/Jump to:/ {//!p}' /tmp/total$i.html > /tmp/filtro.html
    sed -E "s/<\/?code[^>]*>/'/g" /tmp/filtro.html |\
    sed -nE 's/.* href="([^"])">([^<]*)<\a>:.*a href="([^"]*)">([^<]*)</a>.*/\2 '$url_base'\1\n\4 '$url_base'\3/p' >> bc-cache.txt
    # tr ':' '\n' < /tmp/filtro.html | grep -P 'href=".*?">\K.*?(?=</a>)' > bc-titles.txt
    # tr ':' '\n' < /tmp/filtro.html | grep -Po '(?<=href=").*?(?=">)' | xargs -i echo $url_base{} >> bc-titles.txt

done

grep -oP '.*(?= https:.*)' < bc-cache.txt >> bc-titles.txt
grep -oP '.*\Khttps:.*' < bc-cache.txt >> bc-links.txt
