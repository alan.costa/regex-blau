
## ***ROBERTO PIVA:***
**Sigo as labaredas memoráveis dos dias de luto e melancolia**

## ***MAIAKOVSKI***
***A todos vocês,que eu amei e que eu amo, ícones guardados num coração-caverna, como quem num banquete ergue a taça e celebra, repleto de versos levanto meu crânio.***

### *CONCEIÇÃO EVARISTO*
*Nem todo viandante anda estradas, há mundos submersos, que só o silêncio da poesia penetra.*






